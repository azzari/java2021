package modulo_3y4;

import java.util.Scanner;

public class Punto_15 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		char clase;
		
		System.out.print("Ingresar categoria auto ----> ");
		clase = scan.next().charAt(0);
		
		switch(clase) {
		case 'a': case 'A':
			System.out.println("La categor�a "+clase+" cuenta con ----> ");
			System.out.println("\t4 ruedas\tUn motor");
			break;
		case 'b': case 'B':
			System.out.println("La categor�a "+clase+" cuenta con ----> ");
			System.out.println("\t4 ruedas\tUn motor\tSistema de cerradura centralizada\tSistema de aire acondicionado");
			break;
		case 'c': case 'C':
			System.out.println("La categor�a "+clase+" cuenta con ----> ");
			System.out.println("\t4 ruedas\tUn motor\tSistema de cerradura centralizada\tSistema de aire acondicionado\tSistema de Airbag");
			break;
		default:
			System.out.println("Categor�a inexistente");
			break;
		}
		
		scan.close();

	}

}
