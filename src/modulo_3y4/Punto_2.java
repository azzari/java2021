package modulo_3y4;

import java.util.Scanner;

public class Punto_2 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		int num, resto;
		
		System.out.println("Ingrese un n�mero ----> ");
		num = scan.nextInt();
		
		resto = num%2;

		if(resto==0) {
			System.out.println("El n�mero es par");
		}
		else{
			System.out.println("El n�mero es impar");
		}
		
		scan.close();
	}
}