package modulo_3y4;

import java.util.Scanner;

public class Punto_5 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		int puesto;
		
		System.out.print("Ingrese su puesto ----> ");
		puesto = scan.nextInt();
		
		if(puesto==1) {
			System.out.println(puesto + " usa la medalla de Oro.");
		}
		else if(puesto==2) {
			System.out.println(puesto + " usa la medalla de Plata.");
		}
		else if(puesto==3) {
			System.out.println(puesto + " usa la medalla de Bronce");
		}
		else {
			System.out.println("Siga participando.");
		}
		scan.close();

	}

}
