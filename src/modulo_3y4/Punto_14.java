package modulo_3y4;

import java.util.Scanner;

public class Punto_14 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		int puesto;
		
		System.out.print("�En que puesto salio? ----> ");
		puesto = scan.nextInt();
		
		switch(puesto) {
		case 1:
			System.out.println("Medalla de Oro");
			break;
		case 2:
			System.out.println("Medalla de Plata");
			break;
		case 3:
			System.out.println("Medalla de Bronce");
			break;
		default:
			System.out.println("Siga participando.");
		}
		scan.close();
	}

}
