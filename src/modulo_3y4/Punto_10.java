package modulo_3y4;

import java.util.Scanner;

public class Punto_10 {

	public static void main(String[] args) {
		
Scanner scan = new Scanner(System.in);
		
		int num1, num2, num3;
		
		System.out.print("Ingrese su primer n�mero ----> ");
		num1 = scan.nextInt();
		System.out.print("Ingrese su segundo n�mero ----> ");
		num2 = scan.nextInt();
		System.out.print("Ingrese su tercer n�mero ----> ");
		num3 = scan.nextInt();
		
		if(num1==num2 && num2==num3) {
			System.out.println("Num 1, Num 2 y Num 3 son iguales");
		}
		else if(num1>num2 && num2==num3) {
			System.out.println("Num 1 es el mayor");
		}
		else if(num2>num3 && num1==num3) {
			System.out.println("Num 2 es el mayor");
		}
		else if(num3>num1 && num1==num2) {
			System.out.println("Num 3 es el mayor");
		}
		else if(num1==num2 && num1>num3) {
			System.out.println("Num 1 y Num 2 son los mayores");
		}
		else if(num1==num3 && num3>num2) {
			System.out.println("Num 1 y Num 3 son los mayores");
		}
		else if(num2==num3 && num2>num1) {
			System.out.println("Num 2 y Num 3 son los mayores");
		}
		else if(num1>num2 && num2>num3) {
			System.out.println("Num 1 es el mayor");
		}
		else if(num1<num2 && num2<num3) {
			System.out.println("Num 3 es el mayor");
		}
		else if(num1<num2 && num2>num3) {
			System.out.println("Num 2 es el mayor");
		}
		scan.close();

	}

}
