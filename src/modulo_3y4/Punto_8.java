package modulo_3y4;

import java.util.Scanner;

public class Punto_8 {

	public static void main(String[] args) {


		Scanner scan = new Scanner(System.in);
		
		String jugador1;
		String jugador2;
		
		System.out.println("Piedra (a), Papel (b) o Tijeras (c)");
		System.out.print("Jugador 1 ----> ");
		jugador1 = scan.nextLine();
		System.out.print("Jugador 2 ----> ");
		jugador2 = scan.nextLine();
		
		if(jugador1.equals("a")) {
			if(jugador2.equals("b")) {
				System.out.println("Jugador 2 gana");
			}
			else if(jugador2.equals("c")) {
				System.out.println("Jugador 1 gana");
			}
			else {
				System.out.println("Empate");
			}
		}
		else if(jugador1.equals("b")) {
			if(jugador2.equals("a")) {
				System.out.println("Jugador 1 gana");
			}
			else if(jugador2.equals("c")) {
				System.out.println("Jugador 2 gana");
			}
			else {
				System.out.println("Empate");
			}
		}
		else if(jugador1.equals("c")) {
			if(jugador2.equals("a")) {
				System.out.println("Jugador 2 gana");
			}
			else if(jugador2.equals("b")) {
				System.out.println("Jugador 1 gana");
			}
			else {
				System.out.println("Empate");
			}
		}
		scan.close();

	}

}
