package modulo_3y4;

import java.util.Scanner;

public class Punto_3 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Ingrese el mes: ");
		String mes = scan.nextLine();
		
		if (mes.equals("Febrero")||mes.equals("febrero")) {
			System.out.println(mes + " tiene 28 d�as.");
		}
		else if(mes.equals("Abril")||mes.equals("abril")){ 
			System.out.println(mes + " tiene 30 d�as.");
		}
		else if (mes.equals("Junio")||mes.equals("junio")){
			System.out.println(mes + " tiene 30 d�as.");
		}
		else if (mes.equals("Septiembre")||mes.equals("septiembre")) {
			System.out.println(mes + " tiene 30 d�as.");
		}
		else if (mes.equals("Noviembre")||mes.equals("noviembre")) {
			System.out.println(mes + " tiene 30 d�as.");
		}
		else if (mes.equals("Enero")||mes.equals("enero")) {
			System.out.println(mes + " tiene 31 d�as.");
		}
		else if (mes.equals("Marzo")||mes.equals("marzo")) {
			System.out.println(mes + " tiene 31 d�as.");
		}
		else if (mes.equals("Mayo")||mes.equals("mayo")) {
			System.out.println(mes + " tiene 31 d�as.");
		}
		else if (mes.equals("Julio")||mes.equals("julio")) {
			System.out.println(mes + " tiene 31 d�as.");
		}
		else if (mes.equals("Agosto")||mes.equals("agosto") ){
			System.out.println(mes + " tiene 31 d�as.");
		}
		else if (mes.equals("Octubre")||mes.equals("octubre")) {
			System.out.println(mes + " tiene 31 d�as.");
		}
		else if (mes.equals("Diciembre")||mes.equals("diciembre")) {
			System.out.println(mes + " tiene 31 d�as.");
		}
		else {
			System.out.println("mes incorrecto");
		}
		scan.close();
	}
}