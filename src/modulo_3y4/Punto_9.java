package modulo_3y4;

import java.util.Scanner;

public class Punto_9 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		String jugador1, jugador2;
		
		System.out.println("Piedra (a), Papel (b) o Tijeras (c) (usando and)");
		System.out.print("Jugador 1 ----> ");
		jugador1 = scan.nextLine();
		System.out.print("Jugador 2 ----> ");
		jugador2 = scan.nextLine();
		
		if(jugador1.equals("a") && jugador2.equals("a")) {
			System.out.println("Empate");
		}
		else if(jugador1.equals("b") && jugador2.equals("b")) {
			System.out.println("Empate");
		}
		else if(jugador1.equals("c") && jugador2.equals("c")) {
			System.out.println("Empate");
		}
		else if(jugador1.equals("a") && jugador2.equals("b")) {
			System.out.println("Jugador 2 gana");
		}
		else if(jugador1.equals("a") && jugador2.equals("c")) {
			System.out.println("Jugador 1 gana");
		}
		else if(jugador1.equals("b") && jugador2.equals("a")) {
			System.out.println("Jugador 1 gana");
		}
		else if(jugador1.equals("b") && jugador2.equals("c")) {
			System.out.println("Jugador 2 gana");
		}
		else if(jugador1.equals("c") && jugador2.equals("a")) {
			System.out.println("Jugador 2 gana");
		}
		else if(jugador1.equals("c") && jugador2.equals("b")) {
			System.out.println("Jugador 1 gana");
		}
		scan.close();

	}

}
